fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew cask install fastlane`

# Available Actions
## iOS
### ios release_appstore
```
fastlane ios release_appstore
```
Release To AppStore
### ios release_ipa
```
fastlane ios release_ipa
```
Release IPA and send on Slack
### ios release_pod
```
fastlane ios release_pod
```
Pod release
### ios custom_lane
```
fastlane ios custom_lane
```
Description of what the lane does

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
