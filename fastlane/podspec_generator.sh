#!/bin/bash

version=$1
echo "version = $version"
if [[ $version == "" ]]
then
echo "No version specified"
exit 1
fi

# cd ..
# version='1.0.30'
cat >../DondeVisualSearch.podspec <<EOL
Pod::Spec.new do |s|
	s.name              = 'DondeVisualSearch'
	s.version           = '$version'
	s.summary           = 'Awsome SDK for visual search.'
	s.homepage          = 'https://www.dondesearch.com'

	s.author            = { 'Name' => 'donde@dondesearch.com' }
	s.license           = { :type => 'Apache-2.0', :file => 'LICENSE' }

	s.platform          = :ios
	s.source            = { :http => "http://bitbucket.org/dondefashion/dondevisualsearchsampleapp/get/$version.tar.gz" }

	s.ios.deployment_target = '9.0'
	s.vendored_frameworks = 'Frameworks/DondeVisualSearch.framework'
end
EOL
