//
//  ViewController.swift
//  SampleApp
//
//  Created by Daniel Bachar on 06/03/2018.
//  Copyright © 2018 Donde Fashion. All rights reserved.
//

import UIKit
import DondeVisualSearch

class ViewController: UIViewController {
    
    //MARK: - Views
    fileprivate let itemsColletionView:UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 100.0, height: 120.0)
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false
        
        cv.backgroundColor = .clear
        return cv
    }()
    
    fileprivate let depplinkTestButton:UIButton = {
        let b = UIButton(frame: .zero)
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle("Test Deeplink", for: .normal)
        b.setTitleColor(.blue, for: .normal)
        b.addTarget(self, action: #selector(testDeeplink), for: .touchUpInside)
        
        return b
    }()
    
    fileprivate let fontChangeTestButton:UIButton = {
        let b = UIButton(frame: .zero)
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle("Test font change", for: .normal)
        b.setTitleColor(.red, for: .normal)
        b.addTarget(self, action: #selector(testFontChange), for: .touchUpInside)
        
        return b
    }()
    
    //SDK Provided view
    fileprivate var visualSearchView:VisualSearchView!
    
    fileprivate lazy var debounceSearch = debounce(delay: .milliseconds(200)) { [weak self] in
        self?.search(isNew: true)
    }
    
    static var rand = 0
    var items = [DDItem]()
    var page:Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Adding Visual Search and Items Collection View
        visualSearchView = Donde.visualSearchView(with: self,
                                                  userId: "MyUserId",
                                                  initialSearch: nil)
        //Adding SDK View
        view.addSubview(visualSearchView)
        //Adding Items collection view to present search resutlts
        view.addSubview(itemsColletionView)
        //Adding test buttons
        view.addSubview(depplinkTestButton)
        view.addSubview(fontChangeTestButton)
        //View Map
        let views = ["vsv":visualSearchView as UIView,
                     "icv":itemsColletionView,
                     "dlb":depplinkTestButton,
                     "fcb":fontChangeTestButton]
        
        //Visual Search View Horizontal
        let hVisualSearchConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[vsv]|",
                                                                      options: [],
                                                                      metrics: nil,
                                                                      views: views)
        
        //Item Collection View Horizontal
        let hCollectionViewhConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[icv]|",
                                                                         options: [],
                                                                         metrics: nil,
                                                                         views: views)
        //Buttons Horizontal Constraints
        let hButtonsViewhConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-16-[dlb]-(>=1)-[fcb]-16-|",
                                                                      options: [],
                                                                      metrics: nil,
                                                                      views: views)
        
        
        //General Vertical
        let v1Constraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-60-[vsv(==160)][icv][dlb(==30)]-16-|",
                                                           options: [],
                                                           metrics: nil,
                                                           views: views)
        let v2Constraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|-60-[vsv(==160)][icv][fcb(==30)]-16-|",
                                                           options: [],
                                                           metrics: nil,
                                                           views: views)
        
        //Configure Collection View
        itemsColletionView.delegate = self
        itemsColletionView.dataSource = self
        itemsColletionView.register(ItemCell.self, forCellWithReuseIdentifier: "ItemCell")
        
        //Adding constraints
        view.addConstraints(hVisualSearchConstraints+hCollectionViewhConstraints+v1Constraints+v2Constraints+hButtonsViewhConstraints)
        view.layoutIfNeeded()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func testFontChange() {
        ViewController.rand+=1
        if ViewController.rand % 2 == 0 {
            visualSearchView.brandingFont = UIFont.boldSystemFont(ofSize: 11.0)
        } else {
            visualSearchView.brandingFont = UIFont.systemFont(ofSize: 11.0)
        }
        
    }
    
    @objc func testDeeplink() {
        
        //To get the current deeplink string
        // use visualSearchView.currentSearch
        //Here we are settig it
        //This way we can reset the search
        let _ = ""
        //This way we can set certain (specific) search to show on our product tree
        let deeplink = "main_category_idx=0&domain_idx=0&question_idx=1&q_2=3&q_0=0,2&q_1=2"
        visualSearchView.currentSearch = deeplink
    }
    
    func search(isNew isNewSearch:Bool=false) {
        
        //Canceling Previous runing search
        //        Donde.cancelCurrentSearch()
        
        if isNewSearch {
            page = 0
        }
        
        
        //Optional to chage page limit, the defualt if not set will be 40
        let searchParams = SearchParameters(
            filters: nil,
            paginationOffset: NSNumber(integerLiteral: page),
            paginationLimit: 40,
            sizes: nil,
            maxPrice: nil,
            minPrice: nil,
            sort: .none
        )
        
        Donde.performSearch(with: searchParams) { [unowned self] (length,total,hasMore,items,error)   in
            
            if error != .none {
                return
            }
            guard let newItems = items else {
                return
            }
            
            //Spliting paging from new search
            if self.page == 0 {
                //New Search
                self.items = newItems
                self.itemsColletionView.reloadData()
                self.page+=1
            } else {
                //Pagin Search
                let numOfItemsBefore = self.items.count
                self.items.append(contentsOf: newItems)
                let numOfItemsAfter = self.items.count
                if numOfItemsBefore >= numOfItemsAfter {
                    return
                }
                var indexesToInsert = [IndexPath]()
                for index in numOfItemsBefore...numOfItemsAfter-1 {
                    indexesToInsert.append(IndexPath(item: index, section: 0))
                }
                DispatchQueue.main.async {
                    self.itemsColletionView.performBatchUpdates({
                        self.itemsColletionView.insertItems(at: indexesToInsert)
                    }, completion: { (success) in
                        self.page+=1
                    })
                }
                
            }
        }
    }
    
}

//MARK: Extensions

//MARK: - UIScrolViewDelegate
extension ViewController:UIScrollViewDelegate {
    
    //For Paging
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == itemsColletionView {
            let scrollViewHeight = scrollView.frame.height
            let scrollViewContentSizeHeight = scrollView.contentSize.height
            let scrollOffset = scrollView.contentOffset.y
            if (scrollOffset + scrollViewHeight > scrollViewContentSizeHeight) && (scrollViewContentSizeHeight > 0) {
                self.search(isNew: false)
                
            }
        }
    }
}

//MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension ViewController:UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCell", for: indexPath) as? ItemCell else {
            return UICollectionViewCell()
        }
        cell.contentView.backgroundColor = UIColor.randomColor()
        
        let item = items[indexPath.item]
        if let url = item.customData["Largeimage"] {
            cell.imageView.imageFromUrl(url)
        }
        
        
        return cell
    }
    
}

//MARK: - VisualSearchViewDelegate
extension ViewController:VisualSearchViewDelegate {
    func searchChanged() {
        search(isNew: true)
    }
}
