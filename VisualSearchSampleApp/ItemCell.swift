//
//  ItemCell.swift
//  SampleApp
//
//  Created by Daniel Bachar on 18/03/2018.
//  Copyright © 2018 Donde Fashion. All rights reserved.
//

import Foundation
import UIKit

class ItemCell:UICollectionViewCell {
    //MARK: - Public Vars
    let imageView: UIImageView = {
        let iv = UIImageView(frame: .zero)
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        customInit()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        customInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.imageView.image = nil
    }
    
    fileprivate func customInit() {
        contentView.addSubview(imageView)
        
        let views:[String:UIView] = ["iv":imageView]
        
        let hConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|[iv]|",
                                                          options: [],
                                                          metrics: nil,
                                                          views: views)
        let vConstraints = NSLayoutConstraint.constraints(withVisualFormat: "V:|[iv]|",
                                                          options: [],
                                                          metrics: nil,
                                                          views: views)
        contentView.addConstraints(hConstraints+vConstraints)
    }
}

