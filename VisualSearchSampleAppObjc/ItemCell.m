//
//  ItemCell.m
//  SampleAppObjc
//
//  Created by Daniel Bachar on 27/03/2018.
//  Copyright © 2018 Donde Fashion. All rights reserved.
//

#import "ItemCell.h"

@implementation ItemCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.imageView.contentMode = UIViewContentModeScaleAspectFit;
}

- (void)prepareForReuse
{
    self.imageView.image = nil;
    self.priceLabel.text = nil;
}

- (IBAction)didTapAddToCartButton:(UIButton *)button
{
    if (self.delegate)
    {
        [self.delegate didTapAddToCart:self];
    }
}

@end
