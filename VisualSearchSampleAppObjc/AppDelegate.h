//
//  AppDelegate.h
//  VisualSearchSampleAppObjc
//
//  Created by Daniel Bachar on 25/03/2018.
//  Copyright © 2018 DondeFashion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

