//
//  ItemCell.h
//  SampleAppObjc
//
//  Created by Daniel Bachar on 27/03/2018.
//  Copyright © 2018 Donde Fashion. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ItemCell;

@protocol ItemCellDelegate

- (void)didTapAddToCart:(ItemCell *)itemCell;

@end

@interface ItemCell: UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@property (weak, nonatomic) id<ItemCellDelegate> delegate;

@end
