//
//  PopupHelper.m
//  VisualSearchSampleAppObjc
//
//  Created by Daniel Bachar on 02/08/2018.
//  Copyright © 2018 DondeFashion. All rights reserved.
//

#import "PopupHelper.h"
#import "ItemViewController.h"
#import "SivParametersViewController.h"
@import DondeVisualSearch;


@implementation PopupHelper

+ (void)showSimilarItemVCFrom:(UIViewController*)parentVC withItem:(DDItem*)item
{
    __weak typeof (UIViewController) *wparentVC = parentVC;
    
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SivParametersViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SivParametersViewController"];
    vc.modalPresentationStyle = UIModalPresentationPopover;
    
    __weak typeof(SivParametersViewController) *weakVc = vc;
    
    vc.okCallback = ^ {
        [weakVc dismissViewControllerAnimated:YES completion:nil];
        
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ItemViewController *itemViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"ItemViewController"];
        
        itemViewController.item = item;
        [itemViewController setItem:item];
        [itemViewController setOptions:[[ItemViewControllerOptions alloc] init]];
        
        [itemViewController.options setShowMainFeatures:[weakVc showMainFeatures]];
        [itemViewController.options setShowMainItems:[weakVc showMainItems]];
        [itemViewController.options setShowExtra:[weakVc showExtra]];
        [itemViewController.options setShowExtraFeatures:[weakVc showExtraFeatures]];
        [itemViewController.options setShowExtraItems:[weakVc showExtraItems]];
        
        [wparentVC.navigationController pushViewController:itemViewController animated:YES];
    };
    
    [parentVC presentViewController:vc animated:YES completion:nil];
}

+ (void)showLanguagesAndChange:(UIViewController*)parentVC withLanguages:(NSSet<NSString*>*)languages
{
    NSString *title = @"Choose Desired Language";
    //UIAlertControllerStyleAlert
    __block UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                           message:nil
                                                                    preferredStyle:UIAlertControllerStyleActionSheet];
    
    //Ipad crash fix for sheet
    if (alert.popoverPresentationController != nil) {
        alert.popoverPresentationController.sourceView = parentVC.view;
        alert.popoverPresentationController.sourceRect = CGRectMake(0, 0,
                                                                    parentVC.view.bounds.size.height/2,
                                                                    parentVC.view.bounds.size.width/2);
    }
    
    [languages enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, BOOL * _Nonnull stop) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:obj
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           BOOL success = [Donde setCurrentLanguageWithLanguageCode:obj];
                                                           if (!success) {
                                                               //Error handling
                                                           }
                                                           
                                                       }];
        [alert addAction:action];
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction * _Nonnull action) {
                                                       [alert dismissViewControllerAnimated:YES completion:nil];
                                                   }];
    [alert addAction:cancel];
    [parentVC presentViewController:alert animated:YES completion:nil];
}

@end

