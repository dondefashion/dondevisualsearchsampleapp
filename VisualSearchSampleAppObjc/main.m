//
//  main.m
//  VisualSearchSampleAppObjc
//
//  Created by Daniel Bachar on 25/03/2018.
//  Copyright © 2018 DondeFashion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
