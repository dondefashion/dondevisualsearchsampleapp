//
//  SivParametersViewController.m
//  SampleAppObjc
//
//  Created by Liran Tzairi on 12/23/18.
//  Copyright © 2018 Donde Fashion. All rights reserved.
//

#import "SivParametersViewController.h"
//#import "ItemViewController.h"

@interface SivParametersViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *mainItemTableView;
@property (weak, nonatomic) IBOutlet UITableView *extraItemsTableView;
@property (weak, nonatomic) IBOutlet UIButton *showExtraButton;

@end

@implementation SivParametersViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.mainItemTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"TableViewCell"];
    [self.extraItemsTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"TableViewCell"];
    
    self.mainItemTableView.allowsMultipleSelection = YES;
    self.extraItemsTableView.allowsMultipleSelection = YES;
    
    [self changeExtraItemsSection:self.showExtra];
}

- (IBAction)didTapDisplayExtraItems:(UIButton *)sender
{
    [self changeExtraItemsSection:!self.showExtra];
}

- (void)changeExtraItemsSection:(BOOL)appear
{
    if (appear)
    {
        [self setShowExtra:YES];
        [self.showExtraButton setTitleColor:UIColor.blueColor forState:UIControlStateNormal];
        [self.extraItemsTableView setUserInteractionEnabled:YES];
        [self.extraItemsTableView setAlpha:1];
    }
    else
    {
        [self setShowExtra:NO];
        [self.showExtraButton setTitleColor:UIColor.blackColor forState:UIControlStateNormal];
        [self.extraItemsTableView setUserInteractionEnabled:NO];
        [self.extraItemsTableView setAlpha:0.5];
    }
}

- (IBAction)didTapOkButton:(UIButton *)sender
{
    self.okCallback();
}

- (IBAction)didTapCancelButton:(UIButton *)sender
{
    [self dismissViewControllerAnimated:YES completion:Nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TableViewCell" forIndexPath:indexPath];
    
    switch (indexPath.row)
    {
        case 0:
            cell.textLabel.text = @"Show features";
            if ((tableView == self.mainItemTableView && self.showMainFeatures) || (tableView == self.extraItemsTableView && self.showExtraFeatures))
            {
                [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            break;
            
        case 1:
            cell.textLabel.text = @"Show items";
            if ((tableView == self.mainItemTableView && self.showMainItems) || (tableView == self.extraItemsTableView && self.showExtraItems))
            {
                [tableView selectRowAtIndexPath:indexPath animated:NO scrollPosition:UITableViewScrollPositionNone];
                cell.accessoryType = UITableViewCellAccessoryCheckmark;
            }
            break;
            
        default:
            break;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    switch (indexPath.row)
    {
        case 0:
            if (tableView == self.mainItemTableView)
            {
                [self setShowMainFeatures:YES];
            }
            else
            {
                [self setShowExtraFeatures:YES];
            }
            break;
            
        case 1:
            if (tableView == self.mainItemTableView)
            {
                [self setShowMainItems:YES];
            }
            else
            {
                [self setShowExtraItems:YES];
            }
            break;
            
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
    switch (indexPath.row)
    {
        case 0:
            if (tableView == self.mainItemTableView)
            {
                [self setShowMainFeatures:NO];
            }
            else
            {
                [self setShowExtraFeatures:NO];
            }
            break;
            
        case 1:
            if (tableView == self.mainItemTableView)
            {
                [self setShowMainItems:NO];
            }
            else
            {
                [self setShowExtraItems:NO];
            }
            break;
            
        default:
            break;
    }
}

@end
