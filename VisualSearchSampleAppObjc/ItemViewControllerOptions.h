//
//  ItemViewControllerOptions.h
//  VisualSearchSampleAppObjc
//
//  Created by Liran Tzairi on 12/24/18.
//  Copyright © 2018 DondeFashion. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface ItemViewControllerOptions : NSObject

@property (nonatomic) BOOL showMainFeatures;
@property (nonatomic) BOOL showMainItems;

@property (nonatomic) BOOL showExtra;
@property (nonatomic) BOOL showExtraFeatures;
@property (nonatomic) BOOL showExtraItems;

@end

NS_ASSUME_NONNULL_END
