//
//  ItemViewController.m
//  VisualSearchSampleAppObjc
//
//  Created by Liran Tzairi on 12/24/18.
//  Copyright © 2018 DondeFashion. All rights reserved.
//

#import "ItemViewController.h"
#import "UIImageView+URL.h"
#import "ItemCell.h"
#import "PopupHelper.h"

@import DondeVisualSearch;

@interface ItemViewController () <SimilarItemsControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate, ItemCellDelegate>

@property (weak, nonatomic) IBOutlet UIStackView *stackView;
@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;

@property (nonatomic) UILabel* titleLabel;

@property (nonatomic) SimilarItemsController *mainItemController;
@property (nonatomic) SimilarItemsController *extraItemsController;

@property (weak) UICollectionView *mainItemSearchResultsCollectionView;
@property (weak) UICollectionView *extraItemsSearchResultsCollectionView;

@property (nonatomic) NSArray<DDItem *> *mainItemSearchResults;
@property (nonatomic) NSArray<DDItem *> *extraItemsSearchResults;

@end

@implementation ItemViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupItemImageView];
    
    [self initMainItemElements];
    
    if (self.options.showExtra)
    {
        [self initExtraItemsElements];
    }
}

- (void)setupItemImageView
{
    NSString *urlString = [self.item.customData objectForKey:@"Largeimage"];
    NSURL *url = [NSURL URLWithString:urlString];
    
    [self.itemImageView imageFrom:url];
}

- (void)initMainItemElements
{
    DondeSimilarItemsViewOptions *mainItemOptions = [[DondeSimilarItemsViewOptions alloc] initWithViewType:ViewTypeMainItem displayFeatures:self.options.showMainFeatures displayItems:self.options.showMainItems];
    self.mainItemController = [Donde similarItemsControllerWith:self
                                                         itemId:self.item.customerProductId
                                                         userId:@"MyUserId"
                                                    viewOptions:mainItemOptions];
    
    [self.stackView addArrangedSubview:self.mainItemController.view];
    
    if (!self.options.showMainItems)
    {
        UICollectionView *collectionView = [self createItemsCollectionView];
        [self setMainItemSearchResultsCollectionView:collectionView];
        [self.stackView addArrangedSubview:collectionView];
        
        self.mainItemSearchResults = [NSArray array];
    }
}

- (void)initExtraItemsElements
{
    self.titleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    [self.titleLabel setText:@"    Complete the look"];
    [self.titleLabel setTranslatesAutoresizingMaskIntoConstraints:NO];
    [[self.titleLabel.heightAnchor constraintEqualToConstant:40] setActive:YES];
    [self.titleLabel setFont:[UIFont fontWithName:@"ArialMT" size:14]];
    [self.stackView addArrangedSubview:self.titleLabel];
    
    DondeSimilarItemsViewOptions *extraItemsOptions = [[DondeSimilarItemsViewOptions alloc] initWithViewType:ViewTypeExtraItems displayFeatures:self.options.showExtraFeatures displayItems:self.options.showExtraItems];
    self.extraItemsController = [Donde similarItemsControllerWith:self
                                                           itemId:self.item.customerProductId
                                                           userId:@"MyUserId"
                                                      viewOptions:extraItemsOptions];
    
    [self.stackView addArrangedSubview:self.extraItemsController.view];
    
    if (!self.options.showExtraItems)
    {
        UICollectionView *collectionView = [self createItemsCollectionView];
        [self setExtraItemsSearchResultsCollectionView:collectionView];
        [self.stackView addArrangedSubview:collectionView];
        
        self.extraItemsSearchResults = [NSArray array];
    }
}

- (UICollectionView *)createItemsCollectionView
{
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    [layout setItemSize:CGSizeMake(130, 250)];
    [layout setSectionInset:UIEdgeInsetsMake(10, 0, 10, 0)];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [layout setMinimumLineSpacing:10];
    
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    [collectionView setBackgroundColor:UIColor.clearColor];
    
    UINib *itemCellNib = [UINib nibWithNibName:@"ItemCell" bundle:nil];
    [collectionView registerNib:itemCellNib forCellWithReuseIdentifier:@"ItemCell"];
    
    [collectionView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [[collectionView.heightAnchor constraintEqualToConstant:270] setActive:YES];
    
    return collectionView;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    DDEvent *event = [[DDEvent alloc] initWithEventType:DDEventTypeProductDetailPage  data:@{@"customer_product_id":self.item.customerProductId}];
    [Donde injectEventWithEvent:event];
}

//MARK: - SimilarItemsControllerDelegate

- (void)similarItemsController:(SimilarItemsController *)similarItemsController didFinishLoadingWidgetWith:(enum DondeError)error
{
    if (error == DondeErrorNone)
    {
        return;
    }
    
    if (error == DondeErrorExtraItemsNotFound)
    {
        [self.titleLabel removeFromSuperview];
        [self.extraItemsController.view removeFromSuperview];
        self.extraItemsController = nil;
        self.extraItemsSearchResults = nil;
        [self.extraItemsSearchResultsCollectionView setHidden:YES];
    }
    else
    {
        NSString *message = [NSString stringWithFormat:@"Loading finished with error = %ld",(long)error];
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Error" message:message preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleDefault handler:nil]];
        [self presentViewController:alert animated:YES completion:nil];
    }
}

- (void)similarItemsController:(SimilarItemsController *)similarItemsController didChangeSearch:(BOOL)_
{
    SearchParameters *params = [[SearchParameters alloc] initWithFilters:nil
                                                        paginationOffset:@0
                                                         paginationLimit:@15
                                                                   sizes:nil
                                                                  brands:nil
                                                                maxPrice:nil
                                                                minPrice:nil
                                                                    sort:nil];
    
    [similarItemsController searchItemsWithParameters:params];
}

- (void)similarItemsController:(SimilarItemsController *)similarItemsController didFinishSearchWith:(enum DondeError)error length:(NSInteger)length total:(NSInteger)total hasMore:(BOOL)hasMore items:(NSArray<DDItem *> *)items
{
    if (error != DondeErrorNone)
    {
        return;
    }
    
    NSArray<DDItem *> *filteredItems = [items filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(id  _Nullable evaluatedObject, NSDictionary<NSString *,id> * _Nullable bindings) {
        if ([evaluatedObject isKindOfClass:[DDItem class]])
        {
            DDItem *item = (DDItem*)evaluatedObject;
            return ![item.customerProductId isEqualToString:self.item.customerProductId];
        }
        
        return YES;
    }]];
    
    if (similarItemsController == self.mainItemController && !self.options.showMainItems)
    {
        [self setMainItemSearchResults:filteredItems];
        [self.mainItemSearchResultsCollectionView reloadData];
    }
    else if (similarItemsController == self.extraItemsController && !self.options.showExtraItems)
    {
        [self setExtraItemsSearchResults:filteredItems];
        [self.extraItemsSearchResultsCollectionView reloadData];
    }
}

- (void)similarItemsController:(SimilarItemsController *)similarItemsController didSelect:(DDItem *)item
{
    [self didSelectWithItem:item];
}

- (void)eventFired:(DDEvent *)event
{
    NSLog(@"Got event:\n name - %@ \n data - %@", event.name, event.data);
}

//MARK: - UICollectionViewDataSource

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return (collectionView == self.mainItemSearchResultsCollectionView) ? self.mainItemSearchResults.count : self.extraItemsSearchResults.count;
}

- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView
                                   cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    ItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ItemCell" forIndexPath:indexPath];
    cell.delegate = self;
    
    DDItem *item = (collectionView == self.mainItemSearchResultsCollectionView) ? self.mainItemSearchResults[indexPath.item] : self.extraItemsSearchResults[indexPath.item];
    
    if (item == nil)
    {
        return cell;
    }
    
    //price
    id price = [item.customData objectForKey:@"Sale Price"];
    
    if (price != nil && [price isKindOfClass:[NSString class]])
    {
        cell.priceLabel.text = (NSString*)price;
    }
    
    if (price != nil && [price isKindOfClass:[NSNumber class]])
    {
        cell.priceLabel.text = [(NSNumber*)price stringValue];
    }
    
    //Adding Sizes
    NSMutableString *sizesStr = [[NSMutableString alloc] initWithString:@""];
    
    for (NSInteger i = 0; i < item.sizes.count; i++)
    {
        NSString *s = [item.sizes objectAtIndex:i];
        [sizesStr appendString:@"\n"];
        [sizesStr appendString:s];
    }
    
    if (item.sizes.count > 0)
    {
        if (cell.priceLabel.text.length > 0)
        {
            cell.priceLabel.text = [NSString stringWithFormat:@"price - %@\n sizes: %@",cell.priceLabel.text,[sizesStr copy]];
        }
        else
        {
            cell.priceLabel.text = [NSString stringWithFormat:@"sizes - %@",[sizesStr copy]];
        }
        
    }
    
    //Image
    NSString *urlString = [item.customData objectForKey:@"Largeimage"];
    NSURL *url = [NSURL URLWithString:urlString];
    [cell.imageView imageFrom:url];
    
    return cell;
}

//MARK: - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == self.mainItemSearchResultsCollectionView)
    {
        [self didSelectWithItem:self.mainItemSearchResults[indexPath.item]];
    }
    else
    {
        [self didSelectWithItem:self.extraItemsSearchResults[indexPath.item]];
    }
}

- (void)didSelectWithItem:(DDItem *)item
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ItemViewController *itemViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"ItemViewController"];
    
    [itemViewController setItem:item];
    [itemViewController setOptions:self.options];
    
    [self.navigationController pushViewController:itemViewController animated:YES];
}

//MARK: - <ItemCellDelegate>

- (void)didTapAddToCart:(ItemCell *)itemCell
{
    NSIndexPath *indexPath;
    DDItem *item;
    
    if ((indexPath = [self.mainItemSearchResultsCollectionView indexPathForCell:itemCell]))
    {
        item = self.mainItemSearchResults[indexPath.row];
    }
    else if ((indexPath = [self.extraItemsSearchResultsCollectionView indexPathForCell:itemCell]))
    {
        item = self.extraItemsSearchResults[indexPath.row];
    }
    
    if (item)
    {
        DDEvent *event = [[DDEvent alloc] initWithEventType:DDEventTypeAddToCart  data:@{@"customer_product_id":item.customerProductId}];
        [Donde injectEventWithEvent:event];
    }
}

@end
