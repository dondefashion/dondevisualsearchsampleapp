//
//  ViewController.m
//  VisualSearchSampleAppObjc
//
//  Created by Daniel Bachar on 25/03/2018.
//  Copyright © 2018 DondeFashion. All rights reserved.
//

#import "ViewController.h"
#import <UIKit/UIKit.h>

#import "UIImageView+URL.h"
#import "ItemCell.h"
#import "ItemViewController.h"
#import "PopupHelper.h"
#import "SivParametersViewController.h"

@import DondeVisualSearch;

static NSUInteger randomMe;

typedef NS_ENUM(NSInteger, PickerType) {
    PickerTypeMinPrice,
    PickerTypeMaxPrice,
    PickerTypeSizes
};

@interface ViewController () <UICollectionViewDelegate,UIScrollViewDelegate,UICollectionViewDataSource, VisualSearchViewDelegate,ItemCellDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *visualSearchViewWrapper;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *testDeeplinkButton;
@property (weak, nonatomic) IBOutlet UIButton *testFontButton;
@property (weak, nonatomic) IBOutlet UIButton *clearButton;
@property (weak, nonatomic) IBOutlet UIButton *toggleSortButton;
@property (weak, nonatomic) IBOutlet UIButton *minPriceButton;
@property (weak, nonatomic) IBOutlet UIButton *maxPriceButton;
@property (weak, nonatomic) IBOutlet UIButton *sizesButton;
@property (weak, nonatomic) IBOutlet UILabel *noResultsLabel;
@property (weak, nonatomic) IBOutlet UILabel *categroyAndTotalLabel;
@property (weak, nonatomic) IBOutlet UIButton *languageButton;
@property (weak, nonatomic) IBOutlet UIButton *brandsButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *visualSearchViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIPickerView *picker;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *pickerBottomSpaceConstraint;

@property (nonatomic) NSInteger page;
//For paging calculations
@property (nonatomic) CGFloat lastContentOffset;
@property (nonatomic) NSInteger limit;
@property (nonatomic) NSMutableArray<DDItem*> *items;
@property (nonatomic) VisualSearchView *visualSearchView;

//Filters Testing
@property (nonatomic) NSNumber *maxPrice;
@property (nonatomic) NSNumber *minPrice;
@property (nonatomic) NSNumber *searchSort;
@property (nonatomic) NSArray<NSString *> *availableSizes;
@property (nonatomic) NSMutableArray<NSString*> *searchSizes;
@property (nonatomic) NSArray<NSString *> *searchBrands;
@property (nonatomic) NSMutableDictionary<NSString *, NSArray<NSString *> *> *searchExtraFilters;
@property (nonatomic) PickerType pickerType;

@property (nonatomic) ItemViewControllerOptions *itemViewControllerOptions;

//Search Result Handler
typedef void (^SearchResultCompletionBlock) (BOOL isNew, NSInteger length, NSInteger total, BOOL hasMore, NSArray<DDItem *> * _Nullable newItems, NSDictionary<NSString *, id> * _Nullable __strong rawData,  enum DondeError error);
@property (nonatomic, copy, nonnull) SearchResultCompletionBlock resultHandler;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.visualSearchViewHeightConstraint.constant = self.isCompactView ? 120 : 160;
    
    self.itemViewControllerOptions = [[ItemViewControllerOptions alloc] init];
    self.itemViewControllerOptions.showMainFeatures = YES;
    self.itemViewControllerOptions.showMainItems = YES;
    self.itemViewControllerOptions.showExtra = YES;
    self.itemViewControllerOptions.showExtraFeatures = YES;
    self.itemViewControllerOptions.showExtraItems = YES;
    
    self.page = 0;
    self.limit = 40;
    self.availableSizes = [NSArray arrayWithObjects:@"Small",@"Medium",@"Large", nil];
    self.pickerType = PickerTypeSizes;
    self.searchSizes = [NSMutableArray arrayWithCapacity:3];
    self.searchBrands = [NSArray array];
    randomMe = 0;

    [self.testDeeplinkButton setTitle:@"Test Deeplink" forState:UIControlStateNormal];
    [self.testDeeplinkButton setTitleColor:UIColor.redColor forState:UIControlStateNormal];
    
    [self.testFontButton setTitle:@"Change Font" forState:UIControlStateNormal];
    [self.testFontButton sizeToFit];
    
    [self.clearButton setTitle:@"Clear" forState:UIControlStateNormal];
    [self.clearButton sizeToFit];
    
    [self.toggleSortButton setTitle:@"Sort: None" forState:UIControlStateNormal];
    [self.toggleSortButton sizeToFit];
    
    [self.languageButton setTitle:@"languages" forState:UIControlStateNormal];
    [self.languageButton sizeToFit];
    
    [self.brandsButton setTitle:@"brands" forState:UIControlStateNormal];
    [self.brandsButton sizeToFit];
    
    [self.noResultsLabel setHidden:YES];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(100.0, 100.0);
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    [self.collectionView setCollectionViewLayout:layout];
    
    UINib *n = [UINib nibWithNibName:@"ItemCell" bundle:nil];
    [self.collectionView registerNib:n forCellWithReuseIdentifier:@"ItemCell"];
    
    self.items = [NSMutableArray array];

    [self initResultHandler];
    [self initVisualSearchView];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)initVisualSearchView
{
    //We can also set UserID by - [Donde setCurrentUserId:@"MyUserId"]];
    self.visualSearchView = [Donde visualSearchViewWith:self
                                                 userId:@"MyUserId"
                                          initialSearch:nil
                                          isCompactView:self.isCompactView
                                   showSortAndFilterBar:NO
                                    lockCategoryOptions:self.categoryOption];
    
    [self.visualSearchView setTranslatesAutoresizingMaskIntoConstraints:false];
    
    if (self.categoryOption == nil) {
        self.visualSearchView.currentSearch = @"";
    }
    
    [self.visualSearchViewWrapper addSubview:self.visualSearchView];
    //TODO - configure constraints
    NSArray<NSLayoutConstraint *> *vConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[vsv]|"
                                                                                          options:0
                                                                                          metrics:nil
                                                                                            views:@{@"vsv":self.visualSearchView}];
    NSArray<NSLayoutConstraint *> *hConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[vsv]|"
                                                                                          options:0
                                                                                          metrics:nil
                                                                                            views:@{@"vsv":self.visualSearchView}];
    [self.visualSearchViewWrapper addConstraints:vConstraints];
    [self.visualSearchViewWrapper addConstraints:hConstraints];
    [self.visualSearchViewWrapper layoutIfNeeded];
}

- (void)initResultHandler {
    __weak typeof (self) wself = self;

   self.resultHandler = ^(BOOL isNew,
                          NSInteger length,
                          NSInteger total,
                          BOOL hasMore,
                          NSArray<DDItem *> * _Nullable newItems,
                          NSDictionary<NSString *, id> * _Nullable __strong rawData,
                          enum DondeError error) {
        if (isNew) {
            wself.page = 0;
        }

        //Error Handling:
        //1) SearchCanceled is triggered either by user - Donde.cancelCurrentSearch()
        //                               or by SDK before each performSearch
        // So We don't treat it as an Error, it is just to let user know that the previous Request was Canceled
        if (error == DondeErrorSearchCanceled) {
            //Happens mainly in 2 cases:
            //1) You canceld the current search
            //2) 2 performSearch requeuset were sent in small time interval - recommended to debounce
            return;
        }
        //Handling timeout
        //We can either increase timeout for the SDK
        //We can perform same search again - configure retry mach
        if (error == DondeErrorSearchTimeout) {
            //SDK defualt timeout for search request is 4 seconds
            //The developer can change it - timeout >= 2.0 sec
            // If you get a timeout request you can retry to perform the search
            [wself triggerTimeoutErrorFlow];
            return;
        }
        if (error == DondeErrorNoInternetConnection) {
            //SDK indicate no internet conneciton problem
            [wself triggerNoInternetConnectionFlow];
            return;
        }
        //Other Errors
        if (error != DondeErrorNone) {
            [wself triggerGeneralErrorFlow];
            return;
        }
        //2) If no error, and newItems.count == 0 and we are not paging - we show no results
        if (newItems.count == 0 && wself.page == 0) {
            [wself triggerNoResultsFlow];
            return;
        }
        //Reseting if we were in error mode
        if (wself.collectionView.isHidden) {
            [wself.collectionView setHidden:NO];
            [wself.noResultsLabel setHidden:YES];
        }

        //Updating full results string
        NSString *currentCategoryName = self.visualSearchView.currentCategory.name;
        NSString *title = [NSString stringWithFormat:@"total - %ld, Category - %@", (long)total, currentCategoryName, nil];
        wself.categroyAndTotalLabel.text = title;

        if (wself.page == 0) {
            wself.items = [newItems mutableCopy];
            [wself.collectionView reloadData];
            wself.page+=1;
        } else {
            //Pagin Search
            dispatch_async(dispatch_get_main_queue(), ^{
                NSInteger numOfItemsBefore = [wself.items count];
                [wself.items addObjectsFromArray:newItems];
                NSInteger numOfItemsAfter = [wself.items count];

                if (numOfItemsBefore >= numOfItemsAfter) {
                    return;
                }
                NSMutableArray *indexesToInsert = [NSMutableArray array];
                for (NSInteger i = numOfItemsBefore; i<numOfItemsAfter; i++) {
                    [indexesToInsert addObject:[NSIndexPath indexPathForItem:i inSection:0]];
                }

                [wself.collectionView performBatchUpdates:^{
                    [wself.collectionView insertItemsAtIndexPaths:[indexesToInsert copy]];
                } completion:^(BOOL finished) {
                    wself.page+=1;
                }];
            });
        }
   };

    [Donde setSearchCompletionBlock:self.resultHandler];
}

- (void)search:(BOOL)isNew
{
    if (isNew) {
        self.page = 0;
    }
    
    SearchParameters *params = [[SearchParameters alloc] initWithFilters:self.searchExtraFilters
                                                        paginationOffset:[[NSNumber alloc] initWithInteger:self.page*self.limit]
                                                         paginationLimit:[[NSNumber alloc] initWithInteger:self.limit]
                                                                   sizes:self.searchSizes
                                                                  brands:self.searchBrands
                                                                maxPrice:self.maxPrice
                                                                minPrice:self.minPrice
                                                                    sort:self.searchSort];
    [Donde performSearchWith:params];
}

- (void)triggerNoResultsFlow
{
    [self.noResultsLabel setHidden:NO];
    self.noResultsLabel.text = @"No Results!";
    [self.collectionView setHidden:YES];
}

- (void)triggerNoInternetConnectionFlow
{
    [self.noResultsLabel setHidden:NO];
    self.noResultsLabel.text = @"No Internet Connection!";
    [self.collectionView setHidden:YES];
}

- (void)triggerTimeoutErrorFlow
{
    [self.noResultsLabel setHidden:NO];
    self.noResultsLabel.text = @"Search is Timeout - probably due to slow internet connection..please try again";
    [self.collectionView setHidden:YES];
}

- (void)triggerGeneralErrorFlow
{
    [self.noResultsLabel setHidden:NO];
    self.noResultsLabel.text = @"Error in search";
    [self.collectionView setHidden:YES];
}

- (void)resetSearchParams
{
    self.page = 0;
    [self.searchSizes removeAllObjects];
    self.searchBrands = [NSArray array];
    self.searchSort = nil;
    self.minPrice = nil;
    self.maxPrice = nil;
    [self.toggleSortButton setTitle:@"Sort: None" forState:UIControlStateNormal];
}

- (IBAction)toggleSortTapped {
    int currentSort = (self.searchSort == nil) ? -1 : self.searchSort.intValue;
    int newSort = (currentSort == DondeSortRatingDesc) ? -1 : currentSort + 1;
    NSString *sortDescription = @"";

    switch (newSort) {
        case -1:
            sortDescription = @"None";
            self.searchSort = nil;
            break;
        case DondeSortPriceAsc:
            sortDescription = @"Price ascending";
            self.searchSort = [NSNumber numberWithInt:DondeSortPriceAsc];
            break;
        case DondeSortPriceDesc:
            sortDescription = @"Price descending";
            self.searchSort = [NSNumber numberWithInt:DondeSortPriceDesc];
            break;
        case DondeSortPopularityAsc:
            sortDescription = @"Popularity ascending";
            self.searchSort = [NSNumber numberWithInt:DondeSortPopularityAsc];
            break;
        case DondeSortPopularityDesc:
            sortDescription = @"Popularity descending";
            self.searchSort = [NSNumber numberWithInt:DondeSortPopularityDesc];
            break;
        case DondeSortRatingAsc:
            sortDescription = @"Rating ascending";
            self.searchSort = [NSNumber numberWithInt:DondeSortRatingAsc];
            break;
        case DondeSortRatingDesc:
            sortDescription = @"Rating descending";
            self.searchSort = [NSNumber numberWithInt:DondeSortRatingDesc];
            break;
        default:
            break;
    }

    [self.toggleSortButton setTitle:[NSString stringWithFormat:@"Sort: %@", sortDescription] forState:UIControlStateNormal];
    [self search:true];
}

- (IBAction)pickMinPriceTapped {
    [self showPickerType:PickerTypeMinPrice];
}

- (IBAction)pickMaxPriceTapped {
    [self showPickerType:PickerTypeMaxPrice];
}

- (IBAction)pickSizesTapped {
    [self showPickerType:PickerTypeSizes];
}

- (void)showPickerType:(PickerType)type {
    self.pickerType = type;
    [self.picker reloadAllComponents];
    [self showPicker];
}

- (IBAction)clearButtonTapped:(id)sender {
    //You need to remember to reset your own params!
    [self resetSearchParams];
    self.visualSearchView.currentSearch = @"";
}


- (IBAction)testFontButtonTapped:(id)sender {
    randomMe = randomMe + 1;
    if (randomMe % 2 == 0) {
        self.visualSearchView.brandingFont = [UIFont boldSystemFontOfSize:11.0];
    } else {
        self.visualSearchView.brandingFont = [UIFont systemFontOfSize:11.0];
    }
}

- (IBAction)testDeeplinkButtonTapped:(id)sender
{
    //To get the current deeplink string
    // use visualSearchView.currentSearch
    //Here we are settig it
    //This way we can set certain (specific) search to show on our product tree
    //More Examples:
    //Some Questions and Asnwers should be disabled
    //@"main_category_idx=0&domain_idx=0&question_idx=6&search_stage=1&q_5=9&q_0=0&q_1=2"
    __block UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Deeplink"
                                                                           message:@"type in you deeplink"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
    __weak typeof (self) wself = self;
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Type your deeplink";
    }];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"Set Deeplink"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              NSString *deeplink = alert.textFields.firstObject.text;
                                                              if (deeplink.length > 0) {
                                                                  wself.visualSearchView.currentSearch = deeplink;
                                                              } else {
                                                                  //Default
                                                                  wself.visualSearchView.currentSearch = @"search_stage=1&main_category_idx=0&domain_idx=0&question_idx=1&q_2=3&q_0=0,2&q_1=2";
                                                              }
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    
    [alert addAction:confirmAction];
    [self presentViewController:alert animated:YES completion:nil];
}

- (IBAction)testBrandsButtonTapped:(id)sender
{
    __block UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Brands"
                                                                           message:@"Type in your brands"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
    __weak typeof (self) wself = self;
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.text = @"PLUS,FOREVER21";
    }];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"Set Brands"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              NSString *brands = alert.textFields.firstObject.text;
                                                              if (brands.length > 0) {
                                                                  wself.searchBrands = [brands componentsSeparatedByString:@","];
                                                                  [wself search:true];
                                                              }
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    
    [alert addAction:confirmAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

/*- (IBAction)filterTestingButtonTapped:(id)sender {
    
    if ( (self.maxPrice != nil) && (self.minPrice != nil) && (self.searchSort != nil) ) {
        //We Set Filter - now reset
        self.maxPrice = nil;
        self.minPrice = nil;
        [self.searchSizes removeAllObjects];
        self.searchSort = nil;
        self.searchExtraFilters = nil;
        
        [self.filtersTestingButton setTitle:@"Filters" forState:UIControlStateNormal];
        
    } else {
        //We Didnt set filters - now randomise values
        NSInteger min = arc4random_uniform(10);
        NSInteger max = arc4random_uniform(50);

        self.minPrice = [[NSNumber alloc] initWithInt:(int)10 + min];
        self.maxPrice = [[NSNumber alloc] initWithInt:(int)10 + max];
        //Random Sorting
        NSNumber *sort = [[NSNumber alloc] initWithInt:arc4random_uniform(5)];
        //self.searchSort = [[NSArray alloc] initWithObjects:sort, nil];
        //Mapping Sizes - random
        NSInteger sizesCount = arc4random_uniform(4);
        for (NSInteger i=0;i<sizesCount;i++) {
            //[self.searchSizes addObject:[self.availableSizes objectAtIndex:i]];
        }
        
        [self.filtersTestingButton setTitle:@"No Filters" forState:UIControlStateNormal];
        
        self.searchExtraFilters = [[NSMutableDictionary alloc] init];
        //[self.searchExtraFilters setValue:[[NSArray alloc] initWithObjects:@"festival", @"party", nil] forKey:@"wear_to"];
        //[self.searchExtraFilters setValue:[[NSArray alloc] initWithObjects:@"5", @"6", nil] forKey:@"popularity"];
    }
    
    //Build String
    NSString *sortStr = @"";
    
    switch (self.searchSort[0].intValue) {
        case DondeSortPriceAsc:
            sortStr = @"Price ascending";
            break;
        case DondeSortPriceDesc:
            sortStr = @"Price descending";
            break;
        case DondeSortPopularityAsc:
            sortStr = @"Popularity ascending";
            break;
        case DondeSortPopularityDesc:
            sortStr = @"Popularity descending";
            break;
        case DondeSortRatingAsc:
            sortStr = @"Rating ascending";
            break;
        case DondeSortRatingDesc:
            sortStr = @"Rating descending";
            break;
        default:
            break;
    }
    NSString *title = [NSString stringWithFormat:@"maxPrice = %@, minPrice %@, sort = %@", self.maxPrice, self.minPrice, sortStr];
    NSString *msg = [NSString stringWithFormat:@"sizes = %@", self.searchSizes];
    __block UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                           message:msg
                                                                    preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * closeAction = [UIAlertAction actionWithTitle:@"Close"
                                                           style:UIAlertActionStyleCancel
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             [alert dismissViewControllerAnimated:YES completion:nil];
                                                         }];
    
    //Showing
    [alert addAction:closeAction];
    [self presentViewController:alert animated:YES completion:nil];
    [self search:true];
    
}*/

- (IBAction)changeSDKLanguage:(id)sender {
    NSMutableSet<NSString*> *languages = [[Donde availableLanguages] mutableCopy];
    [languages addObject:@"zh"];//unsupported language
    [PopupHelper showLanguagesAndChange:self withLanguages:languages];
}

- (IBAction)didTapSivParamsButton:(id)sender
{
    UIStoryboard *sb = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    SivParametersViewController *vc = [sb instantiateViewControllerWithIdentifier:@"SivParametersViewController"];
    vc.modalPresentationStyle = UIModalPresentationPopover;
    
    [vc setShowMainFeatures:self.itemViewControllerOptions.showMainFeatures];
    [vc setShowMainItems:self.itemViewControllerOptions.showMainItems];
    [vc setShowExtra:self.itemViewControllerOptions.showExtra];
    [vc setShowExtraFeatures:self.itemViewControllerOptions.showExtraFeatures];
    [vc setShowExtraItems:self.itemViewControllerOptions.showExtraItems];
    
    __weak typeof(SivParametersViewController) *weakVc = vc;
    
    vc.okCallback = ^ {
        [self.itemViewControllerOptions setShowMainFeatures:[weakVc showMainFeatures]];
        [self.itemViewControllerOptions setShowMainItems:[weakVc showMainItems]];
        [self.itemViewControllerOptions setShowExtra:[weakVc showExtra]];
        [self.itemViewControllerOptions setShowExtraFeatures:[weakVc showExtraFeatures]];
        [self.itemViewControllerOptions setShowExtraItems:[weakVc showExtraItems]];
        
        [weakVc dismissViewControllerAnimated:YES completion:nil];
    };
    
    [self presentViewController:vc animated:YES completion:nil];
}

//MARK: - Helpers
- (UIColor*)randomColor
{
    CGFloat hue = (CGFloat)(arc4random() % 256) / 256;// use 256 to get full range from 0.0 to 1.0
    CGFloat saturation = (CGFloat)(arc4random() % 128) / 256 + 0.5;// from 0.5 to 1.0 to stay away from white
    CGFloat brightness = (CGFloat)(arc4random() % 128) / 256 + 0.5;// from 0.5 to 1.0 to stay away from black
    return [[UIColor alloc] initWithHue:hue saturation:saturation brightness:brightness alpha:1];
}

//MARK: - VisualSearchViewDelegate
- (void)searchChanged {
    [self search:true];
}

- (void)searchWithEvent:(DDEvent *)event{
    //NOTE - send analytics
    NSLog(@"Got event:\n name - %@ \n data - %@", event.name, event.data);
}

//MARK: - UIScrolViewDelegate

- (void) scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView == self.collectionView) {
        CGFloat scHeight = scrollView.frame.size.height;
        CGFloat scContentHeight = scrollView.contentSize.height;
        CGFloat scOffset = scrollView.contentOffset.y;
        CGFloat triggerBuffer = 100;
        //Checkingwe are scrolling down - no need to page on scrolling up
        BOOL isScrollingDown = self.lastContentOffset < scOffset;
        
        if ( (scOffset + scHeight > scContentHeight-triggerBuffer) && scContentHeight>0 && isScrollingDown) {
            [self search:NO];//Paging
        }
        //Updating current offset
        self.lastContentOffset = scrollView.contentOffset.y;
    }
}

//MARK: - UICollectionViewDelegate, UICollectionViewDataSource
- (nonnull __kindof UICollectionViewCell *)collectionView:(nonnull UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath {
   
    ItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ItemCell" forIndexPath:indexPath];
    cell.delegate = self;
    DDItem *item = self.items[indexPath.item];
    
    if (item == nil) { return cell; }
    
    //Use Donde's API to notify analytics that an item was viewed.
    //Using Dodne's predefeind event type DDEventTypeItemViewed allows Donde to add relevant information to the event
    DDEvent *event = [[DDEvent alloc] initWithEventType:DDEventTypeItemViewed  data:@{@"customer_product_id":item.customerProductId}];
    [Donde injectEventWithEvent:event];
    
    //price
    id price = [item.customData objectForKey:@"Sale Price"];
    if (price != nil && [price isKindOfClass:[NSString class]]) {
        cell.priceLabel.text = (NSString*)price;
    }
    if (price != nil && [price isKindOfClass:[NSNumber class]]) {
        cell.priceLabel.text = [(NSNumber*)price stringValue];
    }
    
    //Adding Sizes
    NSMutableString *sizesStr = [[NSMutableString alloc] initWithString:@""];
    
    for (NSInteger i=0; i<item.sizes.count; i++) {
        NSString *s = [item.sizes objectAtIndex:i];
        [sizesStr appendString:@"\n"];
        [sizesStr appendString:s];
        
    }
    if (item.sizes.count > 0) {
        if (cell.priceLabel.text.length > 0) {
            cell.priceLabel.text = [NSString stringWithFormat:@"price - %@\n sizes: %@",cell.priceLabel.text,[sizesStr copy]];
        } else {
            cell.priceLabel.text = [NSString stringWithFormat:@"sizes - %@",[sizesStr copy]];
        }
        
    }
    
    //Image
    NSString *urlString = [item.customData objectForKey:@"Largeimage"];
    NSURL *url = [NSURL URLWithString:urlString];
    [cell.imageView imageFrom:url];
    return cell;
    
}

- (NSInteger)collectionView:(nonnull UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.items.count;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    //TODO - open item view controller with relevant item
    if (self.items.count <= indexPath.item) {
        return;
    }
    __block DDItem *item = [self.items objectAtIndex:indexPath.item];
    
    //Use Dodde's API to notify analytics of an item tapped action,
    //Using Donde's predefeind event type DDEventTypeItemClicked allows Donde to add relevant information to the event
    DDEvent *event = [[DDEvent alloc] initWithEventType:DDEventTypeItemClicked  data:@{@"customer_product_id":item.customerProductId}];
    [Donde injectEventWithEvent:event];
    
    [self showItemViewControllerWithItem:item];
    
}

- (void)showItemViewControllerWithItem:(DDItem*)item
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    ItemViewController *itemViewController = [mainStoryboard instantiateViewControllerWithIdentifier:@"ItemViewController"];
    
    itemViewController.item = item;
    itemViewController.options = [[ItemViewControllerOptions alloc] init];
    
    [itemViewController.options setShowMainFeatures:self.itemViewControllerOptions.showMainFeatures];
    [itemViewController.options setShowMainItems:self.itemViewControllerOptions.showMainItems];
    [itemViewController.options setShowExtra:self.itemViewControllerOptions.showExtra];
    [itemViewController.options setShowExtraFeatures:self.itemViewControllerOptions.showExtraFeatures];
    [itemViewController.options setShowExtraItems:self.itemViewControllerOptions.showExtraItems];
    
    [self.navigationController pushViewController:itemViewController animated:YES];
}

//MARK: - <ItemCellDelegate>
-(void) didTapAddToCart:(ItemCell *)itemCell {
    //Use Dodde's API to notify analytics of an item tapped action,
    //Using Donde's predefeind event type DDEventTypeAddToCart allows Donde to add relevant information to the event
    DDItem *item = self.items[[self.collectionView indexPathForCell:itemCell].item];
    DDEvent *event = [[DDEvent alloc] initWithEventType:DDEventTypeAddToCart  data:@{@"customer_product_id":item.customerProductId}];
    [Donde injectEventWithEvent:event];
}

// MARK: - Picker

- (IBAction)pickerDoneButtonPressed {
    [self hidePicker];
    [self search:true];
}

- (void)showPicker {
    [UIView animateWithDuration:0.3 animations:^{
        self.pickerBottomSpaceConstraint.constant = 0;
        [self.view layoutIfNeeded];
    }];
}

- (void)hidePicker {
    [UIView animateWithDuration:0.3 animations:^{
        self.pickerBottomSpaceConstraint.constant = -(self.picker.bounds.size.height+30);
        [self.view layoutIfNeeded];
    }];
}

// MARK: - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    switch (self.pickerType) {
        case PickerTypeMinPrice:
            return 101;
            break;
        case PickerTypeMaxPrice:
            return 101;
            break;
        case PickerTypeSizes:
            return self.availableSizes.count + 1;
            break;
    }
}

// MARK: - UIPickerViewDelegate

- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    switch (self.pickerType) {
        case PickerTypeMinPrice:
            return [NSString stringWithFormat:@"%ld", (long)row-1];
            break;
        case PickerTypeMaxPrice:
            return [NSString stringWithFormat:@"%ld", (long)row-1];
            break;
        case PickerTypeSizes:
            if (row == 0) {
                return @"All";
            } else {
                return self.availableSizes[row-1];
            }
            break;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    switch (self.pickerType) {
        case PickerTypeMinPrice:
            if (row == 0) {
                self.minPrice = nil;
                [self.minPriceButton setTitle:@"Min Price: Any" forState:UIControlStateNormal];
            } else {
                self.minPrice = [NSNumber numberWithInteger:row - 1];
                [self.minPriceButton setTitle:[NSString stringWithFormat:@"Min Price: %d", self.minPrice.intValue] forState:UIControlStateNormal];
            }
            break;
        case PickerTypeMaxPrice:
            if (row == 0) {
                self.maxPrice = nil;
                [self.maxPriceButton setTitle:@"Max Price: Any" forState:UIControlStateNormal];
            } else {
                self.maxPrice = [NSNumber numberWithInteger:row - 1];
                [self.maxPriceButton setTitle:[NSString stringWithFormat:@"Max Price: %d", self.maxPrice.intValue] forState:UIControlStateNormal];
            }
            break;
        case PickerTypeSizes:
            if (row == 0) {
                self.searchSizes = [NSMutableArray array];
                [self.sizesButton setTitle:@"Size: All" forState:UIControlStateNormal];
            } else {
                self.searchSizes = [NSMutableArray arrayWithObject:self.availableSizes[row-1]];
                [self.sizesButton setTitle:[NSString stringWithFormat:@"Size: %@", self.availableSizes[row-1]] forState:UIControlStateNormal];
            }
            break;
    }
}


@end


