//
//  MainViewController.m
//  VisualSearchSampleAppObjc
//
//  Created by Daniel Bachar on 29/05/2018.
//  Copyright © 2018 DondeFashion. All rights reserved.
//

#import "MainViewController.h"
#import "ViewController.h"
@import DondeVisualSearch;

@interface MainViewController ()

@property (weak, nonatomic) IBOutlet UISwitch *compactSwitch;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

@end

@implementation MainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSBundle *bundle = [NSBundle bundleWithIdentifier:@"com.dondefashion.DondeVisualSearch"];
    NSString *build = [bundle objectForInfoDictionaryKey: (NSString *)kCFBundleVersionKey];
    NSString *version = [bundle objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    
    _versionLabel.text = [NSString stringWithFormat:@"Version: %@ (%@)", version, build];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)setRequestTimeout:(id)sender {
    
    __block UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Set Request Timeout"
                                                                           message:@"type in the number of seconds you want to set as timeout for server requests - min - 2.0, defualt 6.0"
                                                                    preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.placeholder = @"Type timeout in seconds";
    }];
    UIAlertAction *confirmAction = [UIAlertAction actionWithTitle:@"Set Timeout"
                                                            style:UIAlertActionStyleDefault
                                                          handler:^(UIAlertAction * _Nonnull action) {
                                                              NSString *deeplink = alert.textFields.firstObject.text;
                                                              if (deeplink.length > 0) {
                                                                  //typed
                                                                  NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
                                                                  f.numberStyle = NSNumberFormatterDecimalStyle;
                                                                  NSNumber *myNumber = [f numberFromString:deeplink];
                                                                  NSTimeInterval interval = [myNumber doubleValue];
                                                                  DondeError err = [Donde setSearchTimeout:interval];
                                                                  NSLog(@"%ld" ,(long)err);
                                                              } else {
                                                                  //Default
                                                                  DondeError err = [Donde setSearchTimeout:15.0];
                                                                  NSLog(@"%ld" ,(long)err);
                                                              }
                                                              [alert dismissViewControllerAnimated:YES completion:nil];
                                                          }];
    
    [alert addAction:confirmAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)openDiscoverScreen:(id)sender {
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Test With Category Lock?"
                                                                   message:@"Choose a category from the list provided, or click no if you don't want to test lock categrory"
                                                            preferredStyle:UIAlertControllerStyleActionSheet];
    
    //Ipad crash fix for sheet
    if (alert.popoverPresentationController != nil) {
        alert.popoverPresentationController.sourceView = self.view;
        alert.popoverPresentationController.sourceRect = CGRectMake(0, 0,
                                                                    self.view.bounds.size.height/2,
                                                                    self.view.bounds.size.width/2);
    }
    NSArray<DDCategory*> *availableCategories = [Donde avialableCategories];
    //Mapping categories to buttons
    __weak typeof (self) wself = self;
    [availableCategories enumerateObjectsUsingBlock:^(DDCategory * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:obj.name
                                                         style:UIAlertActionStyleDefault
                                                       handler:^(UIAlertAction * _Nonnull action) {
                                                           UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                           ViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"ViewController"];
                                                           vc.isCompactView = self.compactSwitch.isOn;
                                                           vc.categoryOption = [[DDLockCategoryOptions alloc] initWithCategory:obj navigation:NavigationFlagHide];
                                                           [wself.navigationController pushViewController:vc animated:YES];
                                                           
                                                       }];
      
        [alert addAction:action];
    }];
    
    UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"NO"
                                                           style:UIAlertActionStyleDefault
                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                             UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                             ViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"ViewController"];
                                                             [wself.navigationController pushViewController:vc animated:YES];
                                                             
                                                         }];
    [alert addAction:cancelAction];
    
    if (availableCategories.count > 0) {
        UIAlertAction *hideCategory = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"Test Category Hidden - %@",availableCategories[0].name]
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                                 ViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"ViewController"];
                                                                 vc.isCompactView = self.compactSwitch.isOn;
                                                                 
                                                                 DDLockCategoryOptions *options = [[DDLockCategoryOptions alloc] initWithCategory:availableCategories[0] navigation:NavigationFlagHide];
                                                                 vc.categoryOption = options;
                                                                 [wself.navigationController pushViewController:vc animated:YES];
                                                                 
                                                             }];
        UIAlertAction *multiCategories = [UIAlertAction actionWithTitle:@"Test MultiCategories"
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * _Nonnull action) {
                                                                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                                    ViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"ViewController"];
                                                                    vc.isCompactView = self.compactSwitch.isOn;
                                                                    //Taking only the 2 first categories if there are
                                                                    DDLockCategoryOptions *options = nil;
                                                                    if (availableCategories.count >= 2) {
                                                                        NSArray *c = [NSArray arrayWithObjects:availableCategories[0],availableCategories[1], nil];
                                                                        options = [[DDLockCategoryOptions alloc] initWithCategories:c];
                                                                    }
                                                                    vc.categoryOption = options;
                                                                    [wself.navigationController pushViewController:vc animated:YES];
                                                                    
                                                                }];
        UIAlertAction *enabledCategory = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"Test Category Enabled - %@",availableCategories[0].name]
                                                                  style:UIAlertActionStyleDefault
                                                                handler:^(UIAlertAction * _Nonnull action) {
                                                                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                                                                    ViewController *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"ViewController"];
                                                                    vc.isCompactView = self.compactSwitch.isOn;
                                                                    
                                                                    DDLockCategoryOptions *options = [[DDLockCategoryOptions alloc] initWithCategory:availableCategories[0] navigation:NavigationFlagEnabeled];
                                                                    
                                                                    vc.categoryOption = options;
                                                                    [wself.navigationController pushViewController:vc animated:YES];
                                                                    
                                                                }];
        [alert addAction:enabledCategory];
        [alert addAction:hideCategory];
        [alert addAction:multiCategories];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
    
    
}

@end
