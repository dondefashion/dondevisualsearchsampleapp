//
//  PopupHelper.h
//  VisualSearchSampleAppObjc
//
//  Created by Daniel Bachar on 02/08/2018.
//  Copyright © 2018 DondeFashion. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class DDItem;

@interface PopupHelper : NSObject

+ (void)showSimilarItemVCFrom:(UIViewController*)parentVC withItem:(DDItem*)item;
+ (void)showLanguagesAndChange:(UIViewController*)parentVC withLanguages:(NSSet<NSString*>*)languages;


@end
