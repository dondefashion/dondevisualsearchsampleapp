//
//  ViewController.h
//  VisualSearchSampleAppObjc
//
//  Created by Daniel Bachar on 25/03/2018.
//  Copyright © 2018 DondeFashion. All rights reserved.
//

#import <UIKit/UIKit.h>

@class DDLockCategoryOptions;

@interface ViewController : UIViewController

@property (nonatomic) DDLockCategoryOptions *categoryOption;
@property (nonatomic) BOOL isCompactView;

@end

