//
//  ItemViewController.h
//  VisualSearchSampleAppObjc
//
//  Created by Liran Tzairi on 12/24/18.
//  Copyright © 2018 DondeFashion. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ItemViewControllerOptions.h"

NS_ASSUME_NONNULL_BEGIN

@class DDItem;

@interface ItemViewController : UIViewController

@property (nonatomic) DDItem *item;
@property (nonatomic) NSInteger sivInitOption;
@property (nonatomic) ItemViewControllerOptions *options;

@end

NS_ASSUME_NONNULL_END
