//
//  UIImage+URL.m
//  SampleAppObjc
//
//  Created by Daniel Bachar on 27/03/2018.
//  Copyright © 2018 Donde Fashion. All rights reserved.
//

#import "UIImageView+URL.h"

@implementation UIImageView (URL)

- (void)imageFrom:(NSURL *)url
{
    //use the object's tag in order to prevent updates by an irrelevant url
    self.tag += 1;
    NSInteger lastUrlId = self.tag;

    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error != nil) { return; }
        __weak typeof (self) wself = self;
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data != nil && self.tag == lastUrlId) {
                wself.image = [UIImage imageWithData:data];
            }
        });
    }];
    
    [task resume];
}
@end
