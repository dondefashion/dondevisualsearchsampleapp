//
//  UIImage+URL.h
//  SampleAppObjc
//
//  Created by Daniel Bachar on 27/03/2018.
//  Copyright © 2018 Donde Fashion. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (URL)

- (void)imageFrom:(NSURL *)url;

@end
