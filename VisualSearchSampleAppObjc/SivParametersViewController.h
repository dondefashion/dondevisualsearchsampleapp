//
//  SivParametersViewController.h
//  SampleAppObjc
//
//  Created by Liran Tzairi on 12/23/18.
//  Copyright © 2018 Donde Fashion. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SivParametersViewController : UIViewController

@property (nonatomic) void (^okCallback)(void);

@property (nonatomic) BOOL showMainFeatures;
@property (nonatomic) BOOL showMainItems;

@property (nonatomic) BOOL showExtra;
@property (nonatomic) BOOL showExtraFeatures;
@property (nonatomic) BOOL showExtraItems;

@end

NS_ASSUME_NONNULL_END
